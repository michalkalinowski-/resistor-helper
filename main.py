from collections import deque
from time import process_time

import cProfile, pstats, io
from pstats import SortKey

one_percent_values = [10.0, 10.2, 10.5, 10.7, 11.0, 11.3, 11.5, 11.8, 12.1, 12.4, 12.7, 13.0, 13.3, 13.7, 14.0, 14.3, 14.7, 15.0, 15.4, 15.8, 16.2, 16.5, 16.9, 17.4, 17.8, 18.2, 18.7, 19.1, 19.6, 20.0, 20.5, 21.0, 21.5, 22.1, 22.6, 23.2, 23.7, 24.3, 24.9, 25.5, 26.1, 26.7, 27.4, 28.0, 28.7, 29.4, 30.1, 30.9, 31.6, 32.4, 33.2, 34.0, 34.8, 35.7, 36.5, 37.4, 38.3, 39.2, 40.2, 41.2, 42.2, 43.2, 44.2, 45.3, 46.4, 47.5, 48.7, 49.9, 51.1, 52.3, 53.6, 54.9, 56.2, 57.6, 59.0, 60.4, 61.9, 63.4, 64.9, 66.5, 68.1, 69.8, 71.5, 73.2, 75.0, 76.8, 78.7, 80.6, 82.5, 84.5, 86.6, 88.7, 90.9, 93.1, 95.3, 97.6]
two_percent_values = [10.0, 10.5, 11.0, 11.5, 12.1, 12.7, 13.3, 14.0, 14.7, 15.4, 16.2, 16.9, 17.8, 18.7, 19.6, 20.5, 21.5, 22.6, 23.7, 24.9, 26.1, 27.4, 28.7, 30.1, 31.6, 33.2, 34.8, 36.5, 38.3, 40.2, 42.2, 44.2, 46.4, 48.7, 51.1, 53.6, 56.2, 59.0, 61.9, 64.9, 68.1, 71.5, 75.0, 78.7, 82.5, 86.6, 90.9, 95.3]
five_percent_values = [10, 11, 12, 13, 15, 16, 18, 20, 22, 24, 27, 30, 33, 36, 39, 43, 47, 51, 56, 62, 68, 75, 82, 91]

multipliers = [1, 10, 100, 1000, 10000]

debug_values = [10, 11, 12, 13]

def profile(method):
	def wrapper(*args, **kwargs):
		pr = cProfile.Profile()
		pr.enable()
		result = method(*args, **kwargs)
		pr.disable()
		s = io.StringIO()
		sortby = SortKey.CUMULATIVE
		ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
		ps.print_stats()
		print(s.getvalue())
		return result
	return wrapper

class Node:
    conn_series   = "SERIES"
    conn_parallel = "PARALLEL"

    def __init__(self, resistor_value, parent, connection_type, level):
        self.resistor_value = resistor_value
        self.connection_type = connection_type
        self.level = level

        # description
        if parent:
            symbols = {Node.conn_parallel: " || ", Node.conn_series: " - ", None: ""}
            # sort node values so that the greater one always comes first
            if resistor_value > parent.branch_value:
                self.description = str(resistor_value) + symbols[connection_type] + parent.description
            else:
                self.description = parent.description + symbols[connection_type] + str(resistor_value)
        else:
        	self.description =  str(resistor_value)

        # total value of the network
        if connection_type == Node.conn_series:
            self.branch_value = parent.branch_value + resistor_value
        elif connection_type == Node.conn_parallel:
            self.branch_value = 1 / ( 1/parent.branch_value + 1/resistor_value)
        else:
            self.branch_value = resistor_value

    def __hash__(self):
        return hash(self.description)

    def __repr__(self):
        return self.description

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.description == other.description
        return False
        
@profile
def findValue(target_value, available_values, max_number_res):
    # setup search space with all available values
    nodes_to_search = deque([Node(x, None, None, 1) for x in availableValues])

    results = []
    visited_nodes = set()

    # bfs
    while nodes_to_search:

        current_node = nodes_to_search.popleft()
        if current_node in visited_nodes:
            continue

        visited_nodes.add(current_node)

        if current_node.branch_value == target_value or current_node.level == max_number_res:
            # if exact match or already used max number of resistors then stop searching
            results.append(current_node)
        else:
            # add all possible series and parallel connections
            # WARNING: list comprehensions were slow!
            next_nodes = []
            for value in availableValues:
                next_nodes.append(Node(value, current_node, Node.conn_series, current_node.level + 1))
                next_nodes.append(Node(value, current_node, Node.conn_parallel, current_node.level + 1))
            nodes_to_search.extend(next_nodes)

    print("number of visited nodes: ", len(visited_nodes))
    results = [(abs(target_value - x.branch_value), x) for x in results]
    results.sort(key=lambda x: x[0]) # sorting by first tuple value by default
    return results[:10]

# Inputs
availableValues = [round(value * multiplier, 1) for value in five_percent_values for multiplier in multipliers]
max_number_res = 3
target_value = 1333

if __name__ == "__main__":
	print(len(availableValues), "available values")
	print("target value:", target_value)

	values = findValue(target_value, availableValues, max_number_res)

	for value in values:
	    print(value)
