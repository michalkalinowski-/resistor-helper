Swap array for deque
-----

240 available values
number of visited nodes:  18842632
(0.0, 10000.0 - 301.0)
(0.0, 10000.0 - 105.0 - 196.0)
(0.0, 10000.0 - 14.0 - 287.0)
(0.0, 10000.0 - 147.0 - 154.0)
(0.0, 10000.0 - 154.0 - 147.0)
(0.0, 10000.0 - 196.0 - 105.0)
(0.0, 10000.0 - 226.0 - 75.0)
(0.0, 10000.0 - 287.0 - 14.0)
(0.0, 10000.0 - 75.0 - 226.0)
(0.0, 6810.0 - 3480.0 - 11.0)
---
finished in: 206.249594 seconds
         298395575 function calls in 161.661 seconds

   Ordered by: cumulative time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1   26.538   26.538  161.661  161.661 main.py:61(findValue)
 27878160   21.041    0.000   53.494    0.000 main.py:19(__init__)
    58079   11.500    0.000   40.593    0.001 main.py:82(<listcomp>)
 27878160   30.455    0.000   32.453    0.000 main.py:43(make_description)
    58079    7.719    0.000   32.121    0.001 main.py:83(<listcomp>)
        1   17.828   17.828   19.338   19.338 main.py:87(<listcomp>)
 46720792    9.944    0.000   15.333    0.000 main.py:32(__hash__)
        1   11.079   11.079   12.680   12.680 {method 'sort' of 'list' objects}
 18842632    5.380    0.000   10.886    0.000 {method 'add' of 'set' objects}
 46720792    5.389    0.000    5.389    0.000 {built-in method builtins.hash}
  9035528    4.478    0.000    5.305    0.000 main.py:38(__eq__)
 27878160    2.518    0.000    2.518    0.000 {method 'popleft' of 'collections.deque' objects}
 27877920    1.999    0.000    1.999    0.000 {method 'get' of 'dict' objects}
 18784553    1.615    0.000    1.615    0.000 {method 'append' of 'list' objects}
 18784553    1.601    0.000    1.601    0.000 main.py:88(<lambda>)
 18784553    1.511    0.000    1.511    0.000 {built-in method builtins.abs}
  9035528    0.827    0.000    0.827    0.000 {built-in method builtins.isinstance}
    58079    0.240    0.000    0.240    0.000 {method 'extend' of 'collections.deque' objects}
        1    0.000    0.000    0.000    0.000 main.py:63(<listcomp>)
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.len}



Reduced list comprehensions 
-------

240 available values
number of visited nodes:  4734305
(0.0, 953000.0)
(640377.4810537712, 953000.0 || 953000.0 || 909000.0)
(645263.943161634, 953000.0 || 909000.0 || 909000.0)
(645263.943161634, 909000.0 || 953000.0 || 909000.0)
(645626.4432029796, 953000.0 || 953000.0 || 866000.0)
(650000.0, 909000.0 || 909000.0 || 909000.0)
(650351.4345225391, 953000.0 || 909000.0 || 866000.0)
(650351.4345225391, 909000.0 || 953000.0 || 866000.0)
(650351.4345225391, 866000.0 || 953000.0 || 909000.0)
(650954.2835190166, 953000.0 || 953000.0 || 825000.0)
---
finished in: 72.823724 seconds
         144843151 function calls in 63.634 seconds

   Ordered by: cumulative time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1   18.251   18.251   63.634   63.634 main.py:61(findValue)
 13996080   10.991    0.000   27.319    0.000 main.py:19(__init__)
 13996080   15.345    0.000   16.328    0.000 main.py:43(make_description)
 18730385    3.372    0.000    5.196    0.000 main.py:32(__hash__)
        1    3.218    3.218    3.549    3.549 main.py:89(<listcomp>)
  9261775    2.891    0.000    3.547    0.000 main.py:38(__eq__)
  4734305    1.169    0.000    2.359    0.000 {method 'add' of 'set' objects}
        1    1.837    1.837    2.178    2.178 {method 'sort' of 'list' objects}
 18730385    1.824    0.000    1.824    0.000 {built-in method builtins.hash}
 18700987    1.336    0.000    1.336    0.000 {method 'append' of 'list' objects}
 13996080    0.984    0.000    0.984    0.000 {method 'popleft' of 'collections.deque' objects}
 13995840    0.984    0.000    0.984    0.000 {method 'get' of 'dict' objects}
  9261775    0.656    0.000    0.656    0.000 {built-in method builtins.isinstance}
  4705147    0.340    0.000    0.340    0.000 main.py:90(<lambda>)
  4705147    0.331    0.000    0.331    0.000 {built-in method builtins.abs}
    29158    0.104    0.000    0.104    0.000 {method 'extend' of 'collections.deque' objects}
        1    0.000    0.000    0.000    0.000 main.py:63(<listcomp>)
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}



F strings
------

240 available values
number of visited nodes:  4734305
(0.0, 953000.0)
(640377.4810537712, 953000.0  ||  953000.0  ||  909000.0)
(645263.943161634, 953000.0  ||  909000.0  ||  909000.0)
(645263.943161634, 909000.0  ||  953000.0  ||  909000.0)
(645626.4432029796, 953000.0  ||  953000.0  ||  866000.0)
(650000.0, 909000.0  ||  909000.0  ||  909000.0)
(650351.4345225391, 953000.0  ||  909000.0  ||  866000.0)
(650351.4345225391, 909000.0  ||  953000.0  ||  866000.0)
(650351.4345225391, 866000.0  ||  953000.0  ||  909000.0)
(650954.2835190166, 953000.0  ||  953000.0  ||  825000.0)
---
finished in: 75.17973 seconds
         144843151 function calls in 65.698 seconds

   Ordered by: cumulative time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1   18.000   18.000   65.698   65.698 main.py:63(findValue)
 13996080   11.141    0.000   29.684    0.000 main.py:19(__init__)
 13996080   17.566    0.000   18.543    0.000 main.py:43(make_description)
 18730385    3.246    0.000    5.055    0.000 main.py:32(__hash__)
        1    3.292    3.292    3.632    3.632 main.py:92(<listcomp>)
  9261775    2.941    0.000    3.589    0.000 main.py:38(__eq__)
  4734305    1.153    0.000    2.305    0.000 {method 'add' of 'set' objects}
        1    1.847    1.847    2.168    2.168 {method 'sort' of 'list' objects}
 18730385    1.809    0.000    1.809    0.000 {built-in method builtins.hash}
 18700987    1.355    0.000    1.355    0.000 {method 'append' of 'list' objects}
 13995840    0.977    0.000    0.977    0.000 {method 'get' of 'dict' objects}
 13996080    0.956    0.000    0.956    0.000 {method 'popleft' of 'collections.deque' objects}
  9261775    0.648    0.000    0.648    0.000 {built-in method builtins.isinstance}
  4705147    0.341    0.000    0.341    0.000 {built-in method builtins.abs}
  4705147    0.321    0.000    0.321    0.000 main.py:93(<lambda>)
    29158    0.104    0.000    0.104    0.000 {method 'extend' of 'collections.deque' objects}
        1    0.000    0.000    0.001    0.001 main.py:65(<listcomp>)
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}